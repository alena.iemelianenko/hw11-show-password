let form = document.querySelector('.password-form');
let pass = document.querySelector(".pass");
let confPass = document.querySelector(".conf-pass");
let validMessage = document.querySelector('.validMessage');
let faEye = document.querySelectorAll(".fa-eye");
let faEyesSlash = document.querySelectorAll(".fa-eye-slash");
let eyewrapps = document.querySelectorAll(".show-pass"); 

eyewrapps.forEach(eyewrapp => {
    eyewrapp.addEventListener('click', showPass)
})

function showPass () {
    let target = this.getAttribute('data-tagret');

    let passInput = document.querySelector(`[data-tagret*="${target}"]`);

    let eyeNam 
    if (target === 'pass') {
        eyeNam = 0
    } else {eyeNam = 1}

    if (passInput.getAttribute('type') === 'password') {
        passInput.setAttribute('type','text');
        faEye[eyeNam].style.display = "block";
        faEyesSlash[eyeNam].style.display = "none";
    } else { 
        passInput.setAttribute('type','password');
        faEye[eyeNam].style.display = "none";
        faEyesSlash[eyeNam].style.display = "block";
    }
}


form.addEventListener('submit', (e) => {

    if (pass.value.length === 0 || confPass.value.length === 0) {
        validMessage.style.display = "block";
        validMessage.textContent = "Потрібно ввести значення";
        e.preventDefault();

    } else if (pass.value !== confPass.value ) {
        validMessage.style.display = "block";
        validMessage.textContent = "Потрібно ввести однакові значення";
        e.preventDefault();
    } else  {
        validMessage.style.display = "none";
        alert('You are welcome');
    };
});
